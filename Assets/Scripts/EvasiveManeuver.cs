﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class EvasiveManeuver : MonoBehaviour
{

    public Boundary boundary;

    private Rigidbody2D rb;
    private float targetManeuver;
    public float moveSpeed;

    public float dodge;
    public float smoothing;

    public Vector3 startWait;
    public Vector3 maneuverTime;
    public Vector3 maneuverWait;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        StartCoroutine(Evade());
    }

    // Update is called once per frame
    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.z));

        while (true)
        {
            targetManeuver = Random.Range(1, dodge) * -Mathf.Sign(transform.position.x);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.z));
            targetManeuver = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.z));
        }
    }

    void FixedUpdate()
    {
        float newManeuver = Mathf.MoveTowards(rb.velocity.x, targetManeuver, Time.deltaTime * smoothing);
        rb.velocity = new Vector3(newManeuver, moveSpeed, 0.0f);
        rb.position = new Vector2(Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), rb.position.y);

    }
}
