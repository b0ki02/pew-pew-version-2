﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteor_spawn : MonoBehaviour
{

    public GameObject[] meteors;
    public Vector3 spawnValues;

    
    public float startWait; //timer for beginning game spawn
    public float spawnWait; //spawn delay timer before next wave comes to give the player a quick break adjust in inspector for desired result.

    public int waveCount; //Amount of asteroids per wave

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    // Update is called once per frame
    void Update()
    {
        /*
		timer -= Time.deltaTime;
		if (timer <= 0) {
            Vector3 spawnPosition = new Vector3(Random.Range(spawnValues.x, -spawnValues.x), spawnValues.y, spawnValues.z);
            GameObject meteorNo = meteors[Random.Range(0, meteors.Length)]; 
			Instantiate (meteorNo, spawnPosition, transform.rotation);
			timer = delayTimer;
		}
        */
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < waveCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(spawnValues.x, -spawnValues.x), spawnValues.y, spawnValues.z); //randomize position of spawn between x and -x where y and z stays constant
                GameObject meteorNo = meteors[Random.Range(0, meteors.Length)];             //randomizes the array[] to choose between different models
                Instantiate(meteorNo, spawnPosition, transform.rotation);                   //spawns the meteor or enemy
                yield return new WaitForSeconds(spawnWait);                                 //wait time before the next spawn comes
                                                                                            //* To add increasing number of spawns as score progresses higher in game 
            }

        }
    }
}
