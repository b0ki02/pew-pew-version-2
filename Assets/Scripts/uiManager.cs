﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class uiManager : MonoBehaviour {

	public Button[] buttons;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Play () {
		SceneManager.LoadScene ("Level 1");
		Time.timeScale = 1;
	}

	public void Menu () {
		SceneManager.LoadScene ("Menu");
	}

	public void How () {
		SceneManager.LoadScene ("How");
		Time.timeScale = 1;
	}

	public void Credits () {
		SceneManager.LoadScene ("Credits");
		Time.timeScale = 1;
	}

	public void Exit () {
		Application.Quit ();
		Debug.Log("Game Ended");
	}
}
