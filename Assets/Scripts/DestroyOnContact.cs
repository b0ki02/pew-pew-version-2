﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnContact : MonoBehaviour {

    public GameObject playerExplosion;
    public GameObject meteorExplosion;

	// Use this for initialization
	void Start ()
    {
        playerExplosion = GetComponent<GameObject>();
        meteorExplosion = GetComponent<GameObject>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Laser" || other.gameObject.tag == "Player")
        {
            //Instantiate(playerExplosion, transform.position, transform.rotation);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }


    }
}
