﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteor_destroy : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /*void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.tag == "Meteor" || col.gameObject.tag == "Player Laser") // or if(other.tag == "Meteor" || other.tag == "Player Laser")
        {
			Destroy (col.gameObject);
		}

	}*/


    void OnCollisionEnter2D(Collision2D col)
    {
        //if(other.CompareTag("Meteor) || other.CompareTag("Player Laser")) <--for 3D
        //if (other.tag == "Meteor" || other.tag == "Player Laser") for 3D
        if ((col.gameObject.tag == "Destroyer") || (col.gameObject.tag == "Cockpit"))
        {
            return;
        }

        /*if ((col.gameObject.tag == "Meteor") || (col.gameObject.tag == "Laser") || col.gameObject.tag == "Enemy")
        {
            Destroy(col.gameObject);
        }*/

        Destroy(col.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(other.gameObject);
    }

    /*void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Cockpit")
        {
            return;
        }
    }*/


}
