﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class FlashText : MonoBehaviour {
	
	public float timer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
			if (timer >= 0.5) {
				GetComponent<Text> ().enabled = true;
			}
			if (timer >= 1) {
				GetComponent<Text> ().enabled = false;
				timer = 0;
			}		
	}
}
