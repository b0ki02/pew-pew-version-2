﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour {

    private Vector3 direction;
    private float xMin,xMax,yMin,yMax;
    private AudioSource audioSource;
    // private new Rigidbody2D rigidbody;

    public float speed = 5f, nextFire, fireRate;
    public GameObject shoot; //bullet sprite
    public Transform shootSpawn; //parent of the bullet sprite where it will spawn
    public Joystick joystick;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    
        // Boundary
        xMax = Screen.width - 50;
        xMin = 50; 
        yMax = Screen.height + 30;
        yMin = 200;
    }

    // Update is called once per frame
    private void Update() {
        
        direction = joystick.InputDirection;
        
        if(direction.magnitude != 0){
        
            transform.position += direction * speed;
            transform.position = new Vector3(Mathf.Clamp(transform.position.x,xMin,xMax),Mathf.Clamp(transform.position.y,yMin,yMax),0f);//to restric movement of player
        }
    }

    public void Fire () {
        nextFire = Time.time + fireRate;
        Instantiate(shoot, shootSpawn.position, shootSpawn.rotation);
        audioSource.Play();
    }
}
